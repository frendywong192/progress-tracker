export interface Role {
  title: string;
  id: number;
}

export interface ReportByRole {
  roleId: number;
  created_date: string;
}

export interface FilterDetailDate {
  date1: string;
  date2: string;
  id: number;
}
export interface ReportByProject {
  projectId: number;
  created_date: string;
}

export interface ReportByUser {
  userId: number;
  created_date: string;
}

export interface DeleteData {
  id: number;
  reportDate: string;
}

export interface DetailReport {
  userId: number;
  roleId: number;
  projectId: number;
  created_date: string;
}

export interface ReportDataByRole {
  name: string;
  user_id: number;
  project_name: string;
  role_id: number;
  project_id: number;
  total_task: number;
  created_date: string;
}

export interface ReportDataByUser {
  role_name: string;
  role_id: number;
  project_id: number;
  project_name: string;
  total_task: number;
  created_date: string;
}

export interface ReportDataByProject {
  name: string;
  user_id: number;
  role_name: string;
  role_id: number;
  project_id: number;
  total_task: number;
  created_date: string;
}

export interface User {
  email: string;
  password: string;
}

export interface userStatus {
  userId: number;
  projectId: number;
  roleId: number;
}

export interface TaskCount {
  taskCount: number;
}

export interface LoginData {
  access_token: string;
  expires_in: number;
  status: string;
  code: number;
}

export interface UserData {
  id: number;
  title: string;
  email: string;
}

export interface DetailData {
  cardTitle: string;
  cardList: [
    {
      item: string;
    }
  ];
  cardDate: Date;
  cardId: string;
}

export interface AddUser {
  name: string;
  email: string;
}

export interface EditData {
  id: number;
  name: string;
  email: string;
}

export interface SearchData {
  search: string;
}

export interface EditResponse {
  code: number;
  message: string;
}

export interface objectTasks {
  taskName: string;
  taskDescription: string;
  progressTask: number;
}

export interface objectLaporans {
  projectId: number;
  roleId: number;
  objectTask: objectTasks[];
}

export interface SubmitLaporan {
  userId: number;
  created_date: string;
  objectLaporan: objectLaporans[];
}

export interface UserRole {
  rolename: string;
  roleid: number;
}

export interface UserProject {
  projectname: string;
  projectid: number;
}

export interface UserData {
  email: string;
  isadmin: boolean;
  name: string;
  password: string;
  user_id: number;
}
export interface TokenData {
  iat: number;
  exp: number;
  id: UserData[];
}

export interface CardBody {
  projectName: string;
  roleName: string;
  taskName: [];
  taskProgress: [];
}

export interface Card {
  cardTitle: string;
  cardBody: CardBody[];
  cardDate: Date;
  cardId: number;
  status: string;
  haveReason: boolean;
}

export interface UserId {
  id: number;
}

export interface tempLaporan {
  id: number;
  taskName: string;
  taskDescription: string;
  progressTask: number;
  sliderStatus: boolean;
}

export interface TempProject {
  id: number;
  projectId: number;
  roleId: number;
  objectTasks: tempLaporan[];
}

export interface TempFinalSubmit {
  userId: number;
  objectLaporan: TempProject[];
}

export interface ReportDetailTitle {
  user_id: number;
  project_name: string;
  role_id: number;
  role_name: string;
  project_id: number;
  total_task: number;
  created_date: string;
}

export interface ReportDetailSlug {
  created_date: string;
  userId: number;
}

export interface ReportDetailData {
  task_id: number;
  task_name: string;
  task_description: string;
  progress: string;
}

export interface ReportDetailDataSlug {
  created_date: string;
  userId: number;
  roleId: number;
  projectId: number;
}

export interface ReportData {
  ReportDetailTitle: ReportDetailTitle[];
  ReportDetailData: ReportDetailData[];
}

export interface DashboardByTime {
  id: number;
  search: string;
}

export interface EditobjectTasks {
  taskName: string;
  taskDescription: string;
  progressTask: string;
  taskId: number;
}

export interface EditobjectLaporans {
  projectId: number;
  roleId: number;
  objectTask: EditobjectTasks[];
}

export interface EditLaporan {
  userId: number;
  created_date: string;
  objectLaporan: EditobjectLaporans[];
}

export interface ActiveClass {
  class: string;
}
