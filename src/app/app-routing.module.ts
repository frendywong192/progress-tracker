import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { MainPageComponent } from './homepage/main-page/main-page.component';
import { DetailPageComponent } from './homepage/detail-page/detail-page.component';
import { UserDetailpageComponent } from './user-homepage/user-detailpage/user-detailpage.component';
import { UserDashboardComponent } from './user-homepage/user-dashboard/user-dashboard.component';
import { UserLoginPageComponent } from './user-login-page/user-login-page.component';
import { ReportDetailComponent } from './homepage/report-detail/report-detail.component';
import { UserReportpageComponent } from './user-homepage/user-reportpage/user-reportpage.component';
import { UserEditReportComponent } from './user-homepage/user-edit-report/user-edit-report.component';


const routes: Routes = [
  { path : '' , redirectTo : 'login', pathMatch: 'full'},
  { path: 'login', component: UserLoginPageComponent},
  { path: 'login/admin', component: LoginPageComponent },
  { path: 'admin/homepage/:slug' , component: MainPageComponent},
  { path: 'admin/homepage/:slug/:itemId' , component: DetailPageComponent},
  { path: 'admin/homepage/:slug/:itemId/:reportId', component: ReportDetailComponent},
  { path: 'user/dashboard' , component: UserDashboardComponent},
  { path: 'user/homepage/project/:date' , component: UserDetailpageComponent},
  { path: 'user/homepage/reportpage/:slug' , component: UserReportpageComponent},
  { path: 'user/homepage/editreport/:slug' , component: UserEditReportComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
