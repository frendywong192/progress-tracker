import { DeleteData } from './../model/model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { UserData, AddUser, Role, EditData } from '../model/model';

@Injectable({
  providedIn: 'root'
})
export class EditItemService {
  url = 'http://localhost:3000/admin/';

  deleteReportUrl = 'http://localhost:3000/admin/deleteReport';

  addReasonUrl = 'http://localhost:3000/user/addReason';

  constructor(private http: HttpClient) { }

  deleteItem(userData: Role, slug) {
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.url + 'delete' + slug , userData, {headers: reqHeader});
  }

  editItem(userData: EditData, slug) {
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.url + 'edit' + slug , userData , {headers: reqHeader});
  }

  addItem(userData: AddUser, slug) {
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.url + 'add' + slug , userData , {headers: reqHeader});
  }

  deleteReport(data: DeleteData, slug: string){
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.deleteReportUrl + slug , data , {headers: reqHeader});
  }

  addReason(userId: number, date: string){
    let data = {
      id : userId,
      reportDate: date,
    }
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.addReasonUrl , data , {headers: reqHeader});
  }
}
