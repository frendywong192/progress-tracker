import { Card, UserId, userStatus, TaskCount, ReportDetailSlug, ReportDetailTitle, ReportDetailData, ReportDetailDataSlug, DashboardByTime, EditLaporan } from './../model/model';
import { Injectable } from '@angular/core';
import { SubmitLaporan, UserRole, UserProject } from 'src/app/model/model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  // submitLaporanUrl = 'https://backendtematik.herokuapp.com/user/insertReport';
  // getRoleurl = 'https://backendtematik.herokuapp.com/user/getAllRoles';
  // getProjecturl = 'https://backendtematik.herokuapp.com/user/getAllProjects';
  // getDashboardurl = 'https://backendtematik.herokuapp.com/user/dashboardAll';
  submitLaporanUrl = 'http://localhost:3000/user/insertReport';
  getRoleurl = 'http://localhost:3000/user/getAllRoles';
  getProjecturl = 'http://localhost:3000/user/getAllProjects';
  getDashboardurl = 'http://localhost:3000/user/dashboardAll';
  getStatusUrl = 'http://localhost:3000/user/countTask';

  getFilteredDashboardTimeUrl = 'http://localhost:3000/user/dashboardGroupTime';
  getFilteredDashboardProjectUrl = 'http://localhost:3000/user/dashboardSearchProject';

  getReportDetailTitleUrl = 'http://localhost:3000/user/reportDetail';
  getReportDetailDataUrl = 'http://localhost:3000/user/reportDetail/taskList';
  editReportUrl = 'http://localhost:3000/user/editReportUser';
  constructor(private httpClient: HttpClient) { }

  extractData(res){
    return res;
  }

  submitReport(data : SubmitLaporan){
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post(this.submitLaporanUrl, data , {headers: reqHeader})
  }

  submitEditReport(data : EditLaporan){
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post(this.editReportUrl , data , {headers: reqHeader})
  }

  getUdahLakuin(data : userStatus): Observable<TaskCount>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post(this.getStatusUrl, data, {headers: reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getAllProject(): Observable<UserProject[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.get<UserProject[]>(this.getProjecturl, {headers: reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getAllRole(): Observable<UserRole[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.get<UserRole[]>(this.getRoleurl, {headers: reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getAllDashboard(id: UserId): Observable<Card[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<Card[]>(this.getDashboardurl, id , {headers: reqHeader})
    .pipe(
      map(this.extractData)
    )
  }

  getReportDetailTitle(data : ReportDetailSlug): Observable<ReportDetailTitle[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<ReportDetailTitle[]>(this.getReportDetailTitleUrl, data, {headers: reqHeader})
    .pipe(
      map(this.extractData)
    )
  }

  getReportDetailData(data : ReportDetailDataSlug): Observable<ReportDetailData[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<ReportDetailData[]>(this.getReportDetailDataUrl, data, {headers: reqHeader})
    .pipe(
      map(this.extractData)
    )
  }

  getDashboardByTime(data : DashboardByTime): Observable<Card[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<Card[]>(this.getFilteredDashboardTimeUrl, data , {headers: reqHeader})
    .pipe(
      map(this.extractData)
    )
  }

  getDashboardByProyek(data : DashboardByTime): Observable<Card[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<Card[]>(this.getFilteredDashboardProjectUrl, data , {headers: reqHeader})
    .pipe(
      map(this.extractData)
    )
  }
}