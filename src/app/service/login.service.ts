import { Injectable } from '@angular/core';
import { User} from '../model/model';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'http://localhost:3000/admin';
  userUrl = 'http://localhost:3000/login';
  forgetPassUrl = 'http://localhost:3000/user/forgetPassword/'


  data : {
    email: string;
  }
  constructor(
    private http: HttpClient,
    private route: Router
    ) { }

  login(user: User) {
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.url, user, {headers: reqHeader});
  }

  userLogin(user: User){
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.userUrl, user, {headers: reqHeader});
  }

  forgetPassword(data: string){
    this.data = {
      email: data,
    }
    const reqHeader = new HttpHeaders({});
    return this.http.post(this.forgetPassUrl, this.data, {headers: reqHeader});
  }

}
