import { ReportByProject, ReportDataByProject, ReportDataByUser, ReportByUser, ReportByRole, ReportDataByRole, EditobjectTasks, DetailReport, FilterDetailDate } from './../model/model';
import { Injectable } from '@angular/core';
import { Role, DetailData, UserData, EditData, SearchData} from '../model/model';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  data : Role;
  requestUrl1 = 'http://localhost:3000/admin/list';
  requestUrl1Extention = 'Dashboard/';
  requestUrl2 = 'http://localhost:3000/admin/';
  requestUrl2Extention = 'All';
  searchUrl1 = 'http://localhost:3000/admin/search';

  detailFilterDateUrl = 'http://localhost:3000/admin/'

  requestDetailUrl1 = 'http://localhost:3000/admin/detailBy';
  requestDetailDataUrl1 = 'http://localhost:3000/admin/taskList';
  mainPageData = new BehaviorSubject<UserData[]>([]);
  constructor(private httpClient: HttpClient) {
}

  extractData(res) {
    return res;
  }


  getData(slug): Observable<UserData[]> {
    const httpOptions = {
      headers : new HttpHeaders({
      })
    };
    this.httpClient.get<Role[]>(this.requestUrl1 + slug + this.requestUrl1Extention, httpOptions)
    .pipe(
      map(this.extractData)
      ).subscribe((data) => {
        this.mainPageData.next(data);
      })
    console.log(this.mainPageData);
    return this.mainPageData.asObservable();
    }

    getReportData(slug): Observable<UserData[]> {
      const httpOptions = {
        headers : new HttpHeaders({
        })
      };
      return this.httpClient.get<Role[]>(this.requestUrl1 + slug + this.requestUrl1Extention, httpOptions)
      .pipe(
        map(this.extractData)
        )
      }


      getFilteredDate(data: FilterDetailDate, slug: string):Observable<DetailData[]>{
        const reqHeader = new HttpHeaders({});
        return this.httpClient.post<DetailData[]>(this.detailFilterDateUrl + slug, data , {headers: reqHeader})
       .pipe(
        map(this.extractData)
      );
      }

  private handleError(error: Response){
    return Observable.throw(error.statusText);
  }

  getDetailData(id, kind): Observable<DetailData[]>{
    this.data = {
      title : '',
      id : id,
    }
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<DetailData[]>(this.requestUrl2 + kind + this.requestUrl2Extention, this.data , {headers: reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getReportDetailProject(data : ReportByProject, slug: string): Observable<ReportDataByProject[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<ReportDataByProject[]>(this.requestDetailUrl1 + slug, data, {headers : reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getReportDetailUser(data : ReportByUser, slug: string): Observable<ReportDataByUser[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<ReportDataByUser[]>(this.requestDetailUrl1 + slug, data, {headers : reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getReportDetailRole(data : ReportByRole, slug: string): Observable<ReportDataByRole[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<ReportDataByRole[]>(this.requestDetailUrl1 + slug, data, {headers : reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  getReportDetailData(data : DetailReport): Observable<EditobjectTasks[]> {
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post<EditobjectTasks[]>(this.requestDetailDataUrl1, data, {headers : reqHeader})
    .pipe(
      map(this.extractData)
    );
  }

  searchData(key: SearchData ,slug) : Observable<UserData[]>{
    const reqHeader = new HttpHeaders({});
    return this.httpClient.post(this.searchUrl1 + slug, key, {headers : reqHeader})
    .pipe(
      map(this.extractData)
    );
    }
}
