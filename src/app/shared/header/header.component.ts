import { TokenData } from 'src/app/model/model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../service/login.service';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userData : TokenData;
  decoded;
  constructor(private route: Router,
              private router: ActivatedRoute,
              private login: LoginService,
    ) { }

  status: string;
  ngOnInit() {
    this.userData = jwt_decode( localStorage.getItem('userToken'));
  }

  leave(){
    if(this.userData.id[0].isadmin === true){
        localStorage.removeItem('userToken')
        this.route.navigate(['/', 'login', 'admin']);
      } else if (this.userData.id[0].isadmin === false) {
        localStorage.removeItem('userToken')
        this.route.navigate(['/', 'login']);
      }
    }

  goback(){
    if(this.userData.id[0].isadmin === true){
      this.route.navigate(['/', 'admin', 'homepage', 'project']);
    } else if (this.userData.id[0].isadmin === false) {
      this.route.navigate(['/', 'user', 'dashboard']);
    }
  }
}
