import { HeaderComponent } from './../shared/header/header.component';
import { SharedModule } from './../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { DetailPageComponent } from './detail-page/detail-page.component';
import { ReportDetailComponent } from './report-detail/report-detail.component';
import { ModalModule } from 'src/app/modal/modal.module'

@NgModule({
  declarations: [
    MainPageComponent, 
    DetailPageComponent, 
    ReportDetailComponent,
    SharedModule
  ],
  imports: [
    CommonModule,
    ModalModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    MainPageComponent,
    DetailPageComponent
  ]
})
export class HomepageModule { }
