import { FormGroup, FormControl } from '@angular/forms';
import { UserData, FilterDetailDate } from './../../model/model';
import { TokenData } from 'src/app/model/model';
import { LoginService } from './../../service/login.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DetailData } from '../../model/model';
import { FetchDataService } from '../../service/fetch-data.service';
import * as jwt_decode from 'jwt-decode';
import { ExportToCsv } from 'export-to-csv';
@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {

  currentTime : number;
  decoded: TokenData;
  allSlug: string;
  constructor(private route: Router,
              private router: ActivatedRoute,
              private fetchData: FetchDataService) { }
  itemKind: string;
  itemSlug: number;
  data: DetailData[];
  pageTitle: string;

  filterSlug : FilterDetailDate;

  formTanggal : FormGroup;
  informasiData : UserData[];

  ngOnInit() {
    if(localStorage.getItem('userToken')){
      this.initialize();
    } else{
      this.route.navigate(['/','login']);
    } 
  }

  initialize(){
    this.filterSlug = {
      date1 : '',
      date2 : '',
      id: 0,
    }
    this.formTanggal = new FormGroup ({
      tanggalDari : new FormControl(null, {
        updateOn: 'change',
      }),
      tanggalKe : new FormControl(null, {
        updateOn: 'change',
      }),
    });
    this.currentTime = new Date().getTime() / 1000;
    this.decoded= jwt_decode(localStorage.getItem('userToken'));
    this.router.paramMap.subscribe(param => {
      this.itemSlug = + param.get('itemId') ;
      this.itemKind = param.get('slug');
      if (this.itemKind === 'role'){
        this.allSlug = 'Roles';
      } else if(this.itemKind === 'user'){
        this.allSlug = 'Users';
      } else if(this.itemKind === 'project'){
        this.allSlug = 'Projects';
      }
      this.fetchData.getDetailData(this.itemSlug, this.itemKind)
      .subscribe ( data => {
        this.data = data;
        this.getAllData()
      });
    });
    this.filterData();
    if( this.currentTime > this.decoded.exp){
      localStorage.removeItem('userToken');
      this.route.navigate(['/','login']);
    }
  }

  getAllData(){
    this.fetchData.getReportData(this.allSlug)
    .subscribe(data => {
      this.informasiData = data;
      this.informasiData.find( info => {
        if (info.id === this.itemSlug){
          this.pageTitle = info.title;
        }
      })
    })
  }

  filterData(): void {
    this.formTanggal.get('tanggalDari').valueChanges.subscribe((val: any) => {
      this.filterSlug.date1 = val.year + '-' + ('0' + (val.month).toString()).slice(-2) + '-' + ('0' + (val.day).toString()).slice(-2);
      this.filterSlug.id = +this.itemSlug;
      console.log(this.filterSlug);
      if(this.filterSlug.date1 != '' && this.filterSlug.date2 != ''){
      this.fetchData.getFilteredDate(this.filterSlug,  this.itemKind)
      .subscribe(data => {
      this.data = data;
      });
      }
    })

    this.formTanggal.get('tanggalKe').valueChanges.subscribe((val: any) => {
      this.filterSlug.date2 = val.year + '-' + ('0' + (val.month).toString()).slice(-2) + '-' + ('0' + (val.day).toString()).slice(-2);
      this.filterSlug.id = +this.itemSlug;
      console.log(this.filterSlug);
      if(this.filterSlug.date1 != '' && this.filterSlug.date2 != ''){
      this.fetchData.getFilteredDate(this.filterSlug,  this.itemKind)
      .subscribe ( data => {
      this.data = data;
      });
      }
    })
  };

  exportCsv(){
    var data = []
    for(let i = 0; i<this.data.length; i++){
      for(let j=0; j<this.data[i].cardList.length;j++){
        data.push({
          Tanggal: this.data[i].cardDate,
          Nama : this.data[i].cardList
        })
      }
    }

     const options = {
      fieldSeparator:',',
      quoteStrings:'"',
      decimalSeparator:'.',
      showLabels: true,
      showTitle: true,
      title: this.pageTitle,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      filename:this.pageTitle,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(data);
  }
}



  