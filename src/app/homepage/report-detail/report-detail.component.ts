import { EditItemService } from './../../service/edit-item.service';
import { FetchDataService } from './../../service/fetch-data.service';
import { ReportDataByRole, ReportDataByUser, ReportDataByProject, ReportByRole, ReportByProject, ReportByUser, DetailReport, EditobjectTasks, UserData, DeleteData, ActiveClass } from './../../model/model';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.scss']
})
export class ReportDetailComponent implements OnInit {


  deleteData : DeleteData;

  pageTitle: string;
  roleData: ReportDataByRole[];
  userData: ReportDataByUser[];
  projectData: ReportDataByProject[];

  roleDataSlug: ReportByRole;
  projectDataSlug : ReportByProject;
  userDataSlug: ReportByUser;

  allSlug: string;
  informasiData: UserData[];

  class : ActiveClass[] = [];
  tempClass : ActiveClass;

  i: number;
  reportDetailSlug : DetailReport;
  reportDetailData : EditobjectTasks[];
  pageSlug: string;
  pageId: number;
  pageDate: string;
  constructor(
    private route: Router,
    private router: ActivatedRoute,
    private fetchSvc: FetchDataService,
    private editSvc: EditItemService
  ) { }
  currentDate = Date();

  ngOnInit() {
    if(localStorage.getItem('userToken')){
      this.initialize();
    } else{
      this.route.navigate(['/','login']);
    }
  }

  initialize(){
    this.router.paramMap.subscribe(param => {
      this.pageSlug = param.get('slug') ;
      this.pageId = + param.get('itemId');
      this.pageDate = param.get('reportId');
  });
  if(this.pageSlug === 'role') {
    this.allSlug = 'Roles';
    this.roleDataSlug = {
      roleId : this.pageId,
      created_date : this.pageDate,
    }
    this.fetchSvc.getReportDetailRole(this.roleDataSlug, this.pageSlug)
    .subscribe( data => {
      this.roleData = data;
      for (this.i = 0; this.i < this.roleData.length ; this.i++) {
        this.tempClass = {
          class: 'deactivate'
        }
        this.class.push(this.tempClass)
        this.class[0].class = 'activate'
      }
      this.reportDetailSlug = {
        userId : this.roleData[0].user_id,
        roleId : this.pageId,
        projectId: this.roleData[0].project_id,
        created_date: this.pageDate
      }
      this.getAllData();
      this.getDetailDataUser()
    })
  } else if (this.pageSlug === 'project'){
    this.allSlug = 'Projects';
    this.projectDataSlug = {
      projectId : this.pageId,
      created_date: this.pageDate,
    }
    this.fetchSvc.getReportDetailProject(this.projectDataSlug, this.pageSlug)
    .subscribe( data => {
      this.projectData = data;
      for (this.i = 0; this.i < this.projectData.length ; this.i++) {
        this.tempClass = {
          class: 'deactivate'
        }
        this.class.push(this.tempClass)
        this.class[0].class = 'activate'
      }
      this.reportDetailSlug = {
        userId : this.projectData[0].user_id,
        roleId: this.projectData[0].role_id,
        projectId: this.pageId,
        created_date: this.pageDate,
      }
      this.getAllData();
      this.getDetailDataUser()
    })
  } else if (this.pageSlug === 'user'){
    this.allSlug = 'Users';
    this.userDataSlug = {
      userId : this.pageId,
      created_date: this.pageDate,
    }
    this.fetchSvc.getReportDetailUser(this.userDataSlug, this.pageSlug)
    .subscribe( data => {
      this.userData = data;
      for (this.i = 0; this.i < this.userData.length ; this.i++) {
        this.tempClass = {
          class: 'deactivate'
        }
        this.class.push(this.tempClass)
        this.class[0].class = 'activate'
      }
      this.reportDetailSlug = {
        userId : this.pageId,
        roleId : this.userData[0].role_id,
        projectId : this.userData[0].project_id,
        created_date: this.pageDate,
      }
      this.getAllData();
      this.getDetailDataUser()
    })
  }
  }

  getAllData(){
    this.fetchSvc.getReportData(this.allSlug)
    .subscribe(data => {
      this.informasiData = data;
      this.informasiData.find( info => {
        if (info.id === this.pageId){
          this.pageTitle = info.title;
        }
      })
    })
  }


  getDetailDataUser(){
    this.fetchSvc.getReportDetailData(this.reportDetailSlug).subscribe( data => {
      this.reportDetailData = data;
      console.log(this.reportDetailSlug);
    })
  }

  chooseUser(i: number){
    this.reportDetailSlug = {
      userId : this.pageId,
      roleId : this.userData[i].role_id,
      projectId : this.userData[i].project_id,
      created_date: this.pageDate,
    }
    for(this.i = 0 ; this.i < this.class.length ; this.i++){
      this.class[this.i].class = 'deactivate';
    }
    this.class[i].class = 'activate';
    this.getDetailDataUser()
  }

  chooseRole(i: number){
    this.reportDetailSlug = {
      userId : this.roleData[i].user_id,
      roleId : this.pageId,
      projectId : this.roleData[i].project_id,
      created_date: this.pageDate,
    }
    for(this.i = 0 ; this.i < this.class.length ; this.i++){
      this.class[this.i].class = 'deactivate';
    }
    this.class[i].class = 'activate';
    this.getDetailDataUser()
  }

  chooseProject(i: number){
    this.reportDetailSlug = {
      userId : this.projectData[i].user_id,
      roleId : this.projectData[i].role_id,
      projectId : this.pageId,
      created_date: this.pageDate,
    }
    for(this.i = 0 ; this.i < this.class.length ; this.i++){
      this.class[this.i].class = 'deactivate';
    }
    this.class[i].class = 'activate';
    this.getDetailDataUser()
  }

  deleteReport(){
    this.deleteData = {
      id : this.pageId,
      reportDate: this.pageDate,
    }
    this.editSvc.deleteReport(this.deleteData, this.pageSlug).subscribe( (data : any) => {
      this.route.navigate(['/','admin','homepage',this.pageSlug,this.pageId])
    })
  }

  navigate(){
    this.route.navigate(['/','admin','homepage',this.pageSlug,this.pageId]);
  }

  exportCSV(){
    var data = []
    if(this.pageSlug === 'role') {
      console.log(this.roleData)
      console.log(this.reportDetailData[0].taskName)
      for(let i=0; i<this.roleData.length;i++){
          data.push({
            projectName: this.roleData[i].project_name,
            userName: this.roleData[i].name
          })
        }
    }
    if(this.pageSlug === 'user') {
      for(let i=0; i<this.userData.length;i++){
          data.push({
            projectName: this.userData[i].project_name,
            roleName: this.userData[i].role_name
          })
        }
    }
    if(this.pageSlug === 'project') {
      for(let i=0; i<this.projectData.length;i++){
          data.push({
            UserName: this.projectData[i].name,
            roleName: this.projectData[i].role_name
          })
        }
    }
   

  const options = {
    fieldSeparator:',',
    quoteStrings:'"',
    decimalSeparator:'.',
    showLabels: true,
    showTitle: true,
    title:this.pageSlug,
    useTextFile: false,
    useBom: true,
    useKeysAsHeaders: true,
    filename:this.pageSlug,
  };

  const csvExporter = new ExportToCsv(options);
  csvExporter.generateCsv(data);
  }
}