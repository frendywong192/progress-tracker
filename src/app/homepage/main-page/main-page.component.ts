import { TokenData } from './../../model/model';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FetchDataService } from '../../service/fetch-data.service';
import { UserData, SearchData} from '../../model/model';
import { ExportToCsv } from 'export-to-csv';
import { Subscription } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  data : UserData[];
  loadData: Subscription;
  pageSlug;
  pageThrow;
  pageInfo;
  dropMenu1;
  dropMenu2;
  searchInput: SearchData;
  closeResult: string;
  searchForm: FormGroup;
  constructor(private route: Router,
              private router: ActivatedRoute,
              private modalService: NgbModal,
              private fetchData: FetchDataService,
              private activeModal: NgbModal,
              ) {}

  currentTime: number;
  decoded: TokenData;

  ngOnInit() {
    if(localStorage.getItem('userToken')){
      this.searchForm = new FormGroup ({
        searchBox : new FormControl(null, {
          updateOn : 'change',
        }),
      });
      this.initialize();
    } else{
      this.route.navigate(['/','login']);
    } 
  }

  initialize(){
    console.log('hai');
    this.currentTime = new Date().getTime() / 1000;
    this.decoded= jwt_decode(localStorage.getItem('userToken'));
    this.router.paramMap.subscribe(param => {
      this.pageSlug = param.get('slug') ;
      if (this.pageSlug === 'role') {
        this.pageInfo = 'Roles';
        this.pageThrow = 'Role';
        this.dropMenu1 = 'project';
        this.dropMenu2 = 'user';
        this.loadData = this.fetchData.getData(this.pageInfo)
        .subscribe ( data => {
          this.data = data;
          this.resetForm();
        });
      } else if (this.pageSlug === 'project') {
        this.pageInfo = 'Projects';
        this.pageThrow = 'Project';
        this.dropMenu1 = 'role';
        this.dropMenu2 = 'user';
        this.loadData = this.fetchData.getData(this.pageInfo)
        .subscribe ( data => {
          this.data = data;
          this.resetForm();
        });

      } else if (this.pageSlug === 'user') {
        this.pageInfo = 'Users';
        this.pageThrow = 'User';
        this.dropMenu1 = 'project';
        this.dropMenu2 = 'role';
        this.loadData = this.fetchData.getData(this.pageInfo)
        .subscribe(data => {
          this.data = data;
          this.resetForm();
        });
      } else {
        this.route.navigate(['./homepage/project/']);
      }
    });
    this.searchData();
    if( this.currentTime > this.decoded.exp){
      localStorage.removeItem('userToken');
      this.route.navigate(['/','login']);
    }
  }

  onClick(slug) {
    this.route.navigate(['admin/homepage', slug]);
  }

  open(content){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  giveData(id: any) {
    console.log(id);
  }

  dataChange(){
    console.log('jalan');
    this.loadData = this.fetchData.getData(this.pageInfo)
      .subscribe(data => {
        this.data = data;
      });
  }

  resetForm(){
    this.searchForm = new FormGroup ({
      searchBox : new FormControl(null, {
        updateOn : 'change',
      }),
    });
    this.searchData();
  }

  exportCSV(){
    var data = []
    for(let i = 0; i<this.data.length; i++){
       data.push({
         name: this.data[i].title
       })    
     }

    const options = {
      fieldSeparator:',',
      quoteStrings:'"',
      decimalSeparator:'.',
      showLabels: true,
      showTitle: true,
      title: this.pageInfo,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      filename:this.pageInfo,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(data);
  }

  searchData(): void {
    this.searchForm.get('searchBox').valueChanges.subscribe(val => {
      this.searchInput = {
        search : val
      };
      this.loadData = this.fetchData.searchData(this.searchInput, this.pageThrow)
        .subscribe( data => {
          this.data = data;
        })
    })
    console.log(this.data);
  };

}
