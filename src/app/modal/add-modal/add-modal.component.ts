import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AddUser } from '../../model/model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EditItemService } from '../../service/edit-item.service';
import { MainPageComponent } from 'src/app/homepage/main-page/main-page.component';
import { EditResponse } from 'src/app/model/model';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.scss']
})
export class AddModalComponent implements OnInit {
  newData : FormGroup;
  @Input() title: string;
  userData: AddUser;
  status;
  constructor(
    public modal: NgbModal,
    public editService: EditItemService,
    private mainSvc : MainPageComponent
  ) { }

  ngOnInit() {
    this.newData = new FormGroup({
      name : new FormControl(null,{
        updateOn : 'change',
        validators: [Validators.required]
      }),
      email : new FormControl(null,{
        updateOn: 'change',
        validators: [Validators.required]
      }),
    })
  }

  onClick(){
    this.userData = {
      name : this.newData.value.name,
      email : this.newData.value.email,
    };
    this.status = this.editService.addItem(this.userData, this.title)
    .subscribe((data: EditResponse) => {
      if (data.code === 200) {
        this.close();
        this.mainSvc.dataChange();
      }
    });
  }
  close(){
    this.modal.dismissAll();
  }
}
