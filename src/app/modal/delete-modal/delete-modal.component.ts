import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input } from '@angular/core';
import { Role, EditResponse } from 'src/app/model/model'
import { EditItemService } from '../../service/edit-item.service';
import { MainPageComponent } from 'src/app/homepage/main-page/main-page.component';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {
  userData: Role;
  @Input() data: Role;
  @Input() title: string;

  constructor(
    private editService: EditItemService,
    private modal: NgbModal,
    private mainSvc: MainPageComponent
  ) { }

  ngOnInit() {
  }

  delete(data: Role){
    this.userData = {
      title : '',
      id : data.id  ,
    };
    this.editService.deleteItem(this.userData, this.title).subscribe((data : EditResponse) => {
      if (data.code === 200) {
        this.close();
        this.mainSvc.dataChange();
      }
    })
    this.close();
  }

  close(){
    this.modal.dismissAll();
  }
}
