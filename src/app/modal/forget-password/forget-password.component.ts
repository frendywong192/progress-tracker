import { HttpErrorResponse } from '@angular/common/http';
import { LoginService } from './../../service/login.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
  forgetPassword: FormGroup;
  status;
  constructor(
    public modal: NgbModal,
    public forgetSvc: LoginService,
  ) { }

  ngOnInit() {
    this.forgetPassword = new FormGroup({
      email : new FormControl(null,{
        updateOn : 'change',
        validators: [Validators.required]
      }),
    });
  }

  onClick(){
    this.forgetSvc.forgetPassword(this.forgetPassword.value.email).subscribe((data : any) => {
    },
    (err: HttpErrorResponse) => {
      if(err.status === 201){
        this.close()
        alert('Sudah Dikirimkan Email Yang Berisikan Password Baru Anda')
      } else if(err.status === 404){
        alert('Email Yang anda masukkan tidak terdaftar di website ini')
      }
      });
  }

  close(){
    this.modal.dismissAll();
  }
}
