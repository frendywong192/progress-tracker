import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { AddModalComponent } from './add-modal/add-modal.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';


@NgModule({
  declarations: [EditModalComponent, AddModalComponent, DeleteModalComponent, ForgetPasswordComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [EditModalComponent, AddModalComponent, DeleteModalComponent, ForgetPasswordComponent]
})
export class ModalModule { }
