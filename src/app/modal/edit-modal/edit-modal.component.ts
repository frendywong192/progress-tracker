import { Component, OnInit, Input } from '@angular/core';
import { EditData, UserData, EditResponse } from '../../model/model';
import { EditItemService } from '../../service/edit-item.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MainPageComponent } from 'src/app/homepage/main-page/main-page.component';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})

export class EditModalComponent implements OnInit {
  @Input() title: string;
  @Input() data: UserData;

  editForm: FormGroup;
  newUserData: EditData;
  constructor(
    private serviceItem: EditItemService,
    private route: Router,
    public activeModal: NgbActiveModal,
    public modal: NgbModal,
    private mainSvc: MainPageComponent,
  ) { }

  ngOnInit() {
    this.editForm = new FormGroup({
      newName: new FormControl(this.data.title, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      }),
      newEmail: new FormControl(this.data.email, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      }),
    });
  }

  editItem() {
    this.newUserData = {
      id : this.data.id,
      name : this.editForm.value.newName,
      email : this.editForm.value.newEmail,
    },
    this.serviceItem.editItem(this.newUserData, this.title)
    .subscribe((data : EditResponse) => {
      if (data.code === 200) {
        this.close();
        this.mainSvc.dataChange();
      }
    })
  }

  close() {
    this.modal.dismissAll();
  }

}
