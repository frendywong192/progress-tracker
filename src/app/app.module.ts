
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SharedModule } from './shared/shared.module';
import { MainPageComponent } from './homepage/main-page/main-page.component';
import { DetailPageComponent } from './homepage/detail-page/detail-page.component';
import { UserDetailpageComponent } from './user-homepage/user-detailpage/user-detailpage.component';
import { UserDashboardComponent } from './user-homepage/user-dashboard/user-dashboard.component';
import { UserLoginPageComponent } from './user-login-page/user-login-page.component';
import { ReportDetailComponent } from './homepage/report-detail/report-detail.component'
import { ModalModule } from './modal/modal.module';
import { UserReportpageComponent } from './user-homepage/user-reportpage/user-reportpage.component';
import { UserEditReportComponent } from './user-homepage/user-edit-report/user-edit-report.component';

@NgModule({
  declarations: [
    LoginPageComponent,
    MainPageComponent,
    DetailPageComponent,
    ReportDetailComponent,
    UserDetailpageComponent,
    UserDashboardComponent,
    AppComponent,
    UserLoginPageComponent,
    UserReportpageComponent,
    UserEditReportComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('access_token'); },
          whitelistedDomains: [''],
          blacklistedRoutes: ['']
        }
    })
  ],
  providers:
   [NgbActiveModal,
    DatePipe
   ],
  bootstrap: [AppComponent],

})
export class AppModule { }
 