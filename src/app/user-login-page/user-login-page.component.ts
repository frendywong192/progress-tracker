import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User, LoginData } from '../model/model';
import { HttpErrorResponse } from '@angular/common/http';
import { LoginService } from '../service/login.service';
@Component({
  selector: 'app-user-login-page',
  templateUrl: './user-login-page.component.html',
  styleUrls: ['./user-login-page.component.scss']
})

export class UserLoginPageComponent implements OnInit {

  loginForm : FormGroup;
  isLoginErr: boolean = false;
  false: boolean = false;
  userData: User;
  loginData: LoginData = {
    access_token: '',
    expires_in: 0,
    status: '',
    code: 0,
  };

  constructor(
    private route: Router,
    private modalService: NgbModal,
    private loginService : LoginService,
    ) { }

  ngOnInit() {
    localStorage.removeItem('userToken');
    this.loginForm = new FormGroup({
      userName: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      }),
      userPassword: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      }),
    });
  }

  onClick() {
    this.userData = {
      email: this.loginForm.value.userName,
      password: this.loginForm.value.userPassword
    };
    this.loginService.userLogin(this.userData).subscribe((data: any) => {
      localStorage.setItem('userToken', data.access_token);
      if (data.code === 200) {
        this.route.navigate(['user/dashboard']);
      }
    },
    (err: HttpErrorResponse) => {
    this.isLoginErr = true;
    });
  }

  open(content){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

}
