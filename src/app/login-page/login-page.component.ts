import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, LoginData } from '../model/model';
import { LoginService } from '../service/login.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup;
  isLoginErr: boolean = false;
  false: boolean = false;
  userData: User;
  loginData: LoginData = {
    access_token: '',
    expires_in: 0,
    status: '',
    code: 0,
  };
  selectedOption = 'admin';
  constructor(private route: Router, private loginService: LoginService) { }
  ngOnInit() {
    localStorage.removeItem('userToken');
    this.loginForm = new FormGroup({
      userName: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      }),
      userPassword: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      }),
    });
  }

  onClick() {
    this.userData = {
      email: this.loginForm.value.userName,
      password: this.loginForm.value.userPassword
    };
    this.loginService.login(this.userData).subscribe((data: any) => {
      localStorage.setItem('userToken', data.access_token);
      if (data.code === 200) {
        this.route.navigate(['/admin/homepage/project']);
      }
    },
    (err: HttpErrorResponse) => {
    this.isLoginErr = true;
    });
  }
}
