import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditReportComponent } from './user-edit-report.component';

describe('UserEditReportComponent', () => {
  let component: UserEditReportComponent;
  let fixture: ComponentFixture<UserEditReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEditReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
