import {
  TokenData,
  ReportDetailSlug,
  ReportDetailDataSlug,
  ReportDetailTitle,
  ReportDetailData,
  SubmitLaporan,
  objectLaporans,
  EditLaporan,
  objectTasks,
  EditobjectTasks,
  EditobjectLaporans,
  ActiveClass
} from "./../../model/model";
import { UserService } from "./../../service/user.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import * as jwt_decode from "jwt-decode";

@Component({
  selector: "app-user-edit-report",
  templateUrl: "./user-edit-report.component.html",
  styleUrls: ["./user-edit-report.component.scss"]
})
export class UserEditReportComponent implements OnInit {
  activeData: number = 0;
  pageSlug: string;
  currentTime: number;
  decoded: TokenData;
  currentDate: string;
  titleData: ReportDetailTitle[];
  data: ReportDetailData[];
  constructor(
    private route: Router,
    private GetData: UserService,
    private router: ActivatedRoute
  ) {}
  class: ActiveClass[] = [];
  tempClass: ActiveClass;
  finalLaporan: EditLaporan = {
    userId: 0,
    created_date: "",
    objectLaporan: []
  };

  loadedData: {
    titleData: ReportDetailTitle[];
    data: [ReportDetailData[]];
  };
  i: number = 0;
  j: number = 0;
  titleDataSlug: ReportDetailSlug;
  detailDataSlug: ReportDetailDataSlug;

  tempDataKanan: ReportDetailData[];

  tempObjectData: EditobjectLaporans = {
    projectId: 0,
    roleId: 0,
    objectTask: []
  };
  tempObjectTask: ReportDetailData[] = [];
  tempObjectTaskFinal: EditobjectTasks;
  tempObjectTaskArr: EditobjectTasks[] = [];

  tempObjectLaporanArr: EditobjectLaporans[] = [];
  ngOnInit() {
    if (localStorage.getItem("userToken")) {
      this.initialize();
    } else {
      this.route.navigate(["/", "login"]);
    }
  }

  initialize() {
    this.currentTime = new Date().getTime() / 1000;
    this.decoded = jwt_decode(localStorage.getItem("userToken"));
    if (this.currentTime > this.decoded.exp) {
      localStorage.removeItem("userToken");
      this.route.navigate(["/", "login"]);
    }
    this.loadedData = {
      titleData: [],
      data: [[]]
    };
    this.router.paramMap.subscribe(param => {
      this.pageSlug = param.get("slug");
      this.currentDate = this.pageSlug;
      this.getDetailTitle(this.pageSlug, 0);
    });
  }

  getDetailTitle(data: string, loadedData: number) {
    this.titleDataSlug = {
      created_date: data,
      userId: this.decoded.id[0].user_id
    };
    this.GetData.getReportDetailTitle(this.titleDataSlug).subscribe(data => {
      this.loadedData.titleData = data;
      console.log(this.loadedData);
      for (this.i = 0; this.i < this.loadedData.titleData.length; this.i++) {
        this.detailDataSlug = {
          created_date: this.pageSlug,
          userId: this.decoded.id[0].user_id,
          roleId: data[this.i].role_id,
          projectId: data[this.i].project_id
        };
        this.getDetailData(this.detailDataSlug, this.i);
      }
      this.loadedData.data.splice(0, 1);
      for (this.i = 0; this.i < this.loadedData.titleData.length; this.i++) {
        this.tempClass = {
          class: "deactivate"
        };
        this.class.push(this.tempClass);
        this.class[0].class = "activate";
      }
      console.log(this.loadedData);
    });
  }

  getDetailData(data: ReportDetailDataSlug, i: number) {
    this.GetData.getReportDetailData(data).subscribe(report => {
      this.tempDataKanan = report;
      console.log(report);
      this.loadedData.data.push(this.tempDataKanan);
    });
  }

  addNama(taskId: number, id: number, event: any) {
    this.loadedData.data[taskId].find(task => {
      if (task.task_id == id) {
        task.task_name = event.target.value;
        return true;
      }
    });
  }

  addDescription(taskId: number, id: any, event: any) {
    this.loadedData.data[taskId].find(task => {
      if (task.task_id == id) {
        task.task_description = event.target.value;
      }
    });
  }

  addProgress(taskId: number, id: number, event: any) {
    this.loadedData.data[taskId].find(task => {
      if (task.task_id == id) {
        task.progress = event.target.value;
        return true;
      }
    });
  }

  chooseData(i: number) {
    this.activeData = i;
    for (this.i = 0; this.i < this.class.length; this.i++) {
      this.class[this.i].class = "deactivate";
    }
    this.class[i].class = "activate";
  }

  submitLaporan() {
    this.finalLaporan.userId = this.decoded.id[0].user_id;
    this.finalLaporan.created_date = this.currentDate;
    for (this.i = 0; this.i < this.loadedData.titleData.length; this.i++) {
      this.tempObjectTask = this.loadedData.data[this.i];
      for (this.j = 0; this.j < this.tempObjectTask.length; this.j++) {
        this.tempObjectTaskFinal = {
          taskId: this.tempObjectTask[this.j].task_id,
          taskName: this.tempObjectTask[this.j].task_name,
          taskDescription: this.tempObjectTask[this.j].task_description,
          progressTask: this.tempObjectTask[this.j].progress
        };
        this.tempObjectTaskArr.push(this.tempObjectTaskFinal);
      }
      this.tempObjectData.projectId = this.loadedData.titleData[
        this.i
      ].project_id;
      (this.tempObjectData.roleId = this.loadedData.titleData[this.i].role_id),
        (this.tempObjectData.objectTask = this.tempObjectTaskArr);
      this.tempObjectLaporanArr.push(this.tempObjectData);
      this.tempObjectTaskArr = [];
      this.tempObjectData = {
        projectId: 0,
        roleId: 0,
        objectTask: []
      };
    }
    this.finalLaporan.objectLaporan = this.tempObjectLaporanArr;
    this.GetData.submitEditReport(this.finalLaporan).subscribe((data: any) => {
      if (data.code === 201) {
        this.route.navigate(["user/homepage/reportpage", this.pageSlug]);
      }
    });
  }
}
