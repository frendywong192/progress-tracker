import { HttpErrorResponse } from '@angular/common/http';
import { EditItemService } from 'src/app/service/edit-item.service';
import { TokenData, Card, UserId, DashboardByTime } from './../../model/model';
import { UserService } from './../../service/user.service';
import { LoginService } from './../../service/login.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { Time, DatePipe } from '@angular/common';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {
  currentDate : Date;
  currentTime: number;
  slugDate: string;
  jumlahData: number = 3;
  bulan = [{nama: 'Januari', value: 1}, {nama: 'Februari', value: 2},{nama: 'Maret', value: 3},{nama: 'April', value: 4},{nama: 'Mei', value: 5},{nama: 'Juni', value: 6},{nama: 'Juli', value: 7},{nama: 'Agustus', value: 8},{nama: 'September', value: 9},{nama: 'Oktober', value: 10},{nama: 'November', value: 11},{nama: 'Desember', value: 12},]
  tahun = [{nama: 2019},{nama: 2020},{nama: 2021},{nama: 2022},{nama: 2023},]
  constructor(
    private route: Router,
    private userSvc: UserService,
    private editSvc: EditItemService,
    private datePipe: DatePipe
    ) { }
  public show:boolean = false;
  public buttonName:any = 'Show';
  accessToken: TokenData;
  data: Card[];
  userId: UserId;
  selectBulan: number = 0;
  selectTahun: number = 0;
  filter: DashboardByTime;
  hariKosong: number;
  berdasarkan: string = '';
  i = 0;
  ngOnInit() {
    if(localStorage.getItem('userToken')){
      this.initialize();
      return true;
    } else{
      this.route.navigate(['/','login']);
    }
  }

  initialize(){
    this.hariKosong = 0;
    this.currentDate = new Date();
    this.slugDate = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
    this.currentTime = new Date().getTime() / 1000;
    this.accessToken = jwt_decode(localStorage.getItem('userToken'));
    this.userId = {
      id: this.accessToken.id[0].user_id,
    }
    this.userSvc.getAllDashboard(this.userId)
    .subscribe( data => {
      this.data = data.reverse();
      this.getHariKosong();
    })
    if( this.currentTime > this.accessToken.exp){
      localStorage.removeItem('userToken');
      this.route.navigate(['/','login']);
    }
  }
  toggle() {
    this.jumlahData = this.jumlahData + 4;
  }

  changeBulan(event: any){
    this.selectBulan = event.target.value;
    if (this.selectBulan !== 0 && this.selectTahun !== 0){
      this.filter = {
        id: this.accessToken.id[0].user_id,
        search: this.selectBulan + '-' + this.selectTahun,
      }
      this.userSvc.getDashboardByTime(this.filter)
      .subscribe( data => {
        this.data = data.reverse();
      })
    }
  }
  changeTahun(event: any){
    this.selectTahun = event.target.value;
    if (this.selectBulan !== 0 && this.selectTahun !== 0){
      this.filter = {
        id: this.accessToken.id[0].user_id,
        search: this.selectBulan + '-' + this.selectTahun,
      }
      this.userSvc.getDashboardByTime(this.filter)
      .subscribe( data => {
        this.data = data.reverse();
      })
    }
  }

  changeBerdasarkan(event: any){
    this.berdasarkan = event.target.value;
  }
  
  searchProyek(event: any){
    this.filter = {
      id: this.accessToken.id[0].user_id,
      search: event.target.value,
    }
    this.userSvc.getDashboardByProyek(this.filter)
      .subscribe( data => {
        this.data = data.reverse();
      })
  }

  onClick(data: string) {
    this.route.navigate(['/','user','homepage','project', data]);
  }


  detail(data: string){
    this.route.navigate(['user/homepage/reportpage',data])
  }

  getHariKosong(){
    for(this.i = 0; this.i < this.data.length ; this.i++){
      if(this.data[this.i].status === 'empty' && this.data[this.i].haveReason === false){
        this.hariKosong++;
      }
    }
  }

  addReason(id: string){
    this.editSvc.addReason(this.accessToken.id[0].user_id, id).subscribe((data: any) => {
      console.log(data);
      if(data.code === 200){
        this.userSvc.getAllDashboard(this.userId)
        .subscribe( data => {
        this.data = data.reverse();
        this.hariKosong = 0;
        this.getHariKosong();
      });
      }
    },
    (err: HttpErrorResponse) => {
    });
  }

}
