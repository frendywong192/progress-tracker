import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailpageComponent } from './user-detailpage.component';

describe('UserDetailpageComponent', () => {
  let component: UserDetailpageComponent;
  let fixture: ComponentFixture<UserDetailpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
