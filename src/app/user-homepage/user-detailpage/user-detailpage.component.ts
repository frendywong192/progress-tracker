import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './../../service/user.service';
import { objectTasks, objectLaporans, SubmitLaporan, tempLaporan, UserRole, UserProject, TokenData, Card, TempProject, TempFinalSubmit, TaskCount, userStatus, ActiveClass } from './../../model/model';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-user-detailpage',
  templateUrl: './user-detailpage.component.html',
  styleUrls: ['./user-detailpage.component.scss']
})
export class UserDetailpageComponent implements OnInit {

  statusCount : TaskCount;
  statusData : userStatus;

  tempFinal : TempFinalSubmit = {
    userId : 0,
    objectLaporan : [{
      id : 0,
      projectId: 1,
      roleId: 1,
      objectTasks : [{
        id: 1,
        taskName: '',
        taskDescription: '',
        progressTask: 0,
        sliderStatus: false,
      }],
    }]
  }

  class : ActiveClass[] = [];
  tempClass : ActiveClass;

  formRoleId: number;
  formProjectId: number;

  projectName: string;
  roleName: string;
  allRole : UserRole[] = [];
  allProject: UserProject[] = [];
  templaporan: tempLaporan;
  i : number;
  j : number;
  currentDate: string;
  progress = 0;
  tempNumberLaporan1: number = 1;
  tempNumberProject1: number = 1;

  laporan: tempLaporan[] = [{
    id: 1,
    taskName: '',
    taskDescription: '',
    progressTask: 0,
    sliderStatus: false,
  }];

  tempProject: TempProject;
  fixedLaporan: tempLaporan = {
    id: 0,
    taskName: '',
    taskDescription: '',
    progressTask: 0,
    sliderStatus: false,
  };
  tempProjectId: number = 0;
  tempRoleId: number = 0;
  objectLaporan : objectLaporans = {
    projectId: 0,
    roleId: 0,
    objectTask: [],
  };

  tempObjectLaporan: objectLaporans[] = [];

  objectTask : objectTasks = {
    taskName: '',
    taskDescription: '',
    progressTask: 0,
  }

  tempObjectTask: objectTasks[] = [];

  finalLaporan: SubmitLaporan = {
    userId: 0,
    created_date: '',
    objectLaporan: [],
  };
  currentTime: number;
  accessToken: TokenData;

  loadedData: number = 0;

  constructor(
    private submitSvc: UserService,
    private route: Router,
    private router: ActivatedRoute
  ) { }


  ngOnInit() {
    
    if(localStorage.getItem('userToken')){
      this.initialize();
    } else{
      this.route.navigate(['/','login']);
    }

  }

  test(id){
    this.loadedData = id;
    console.log(this.loadedData);
    this.allRole.find(role => {
      if (role.roleid == this.tempFinal.objectLaporan[this.loadedData].roleId){
      this.roleName = role.rolename
      }
    })
    this.allProject.find(project => {
      if (project.projectid == this.tempFinal.objectLaporan[this.loadedData].projectId){
      this.projectName = project.projectname
      }
    })
  }

  initialize(){
    this.submitSvc.getAllRole().subscribe(data => {
      this.allRole = data;
    });
    this.submitSvc.getAllProject().subscribe(data => {
      this.allProject = data;
    });
    this.router.paramMap.subscribe(param => {
    this.currentDate = param.get('date')
    }); 
    this.currentTime = new Date().getTime() / 1000;
    this.accessToken = jwt_decode(localStorage.getItem('userToken'));
    if( this.currentTime > this.accessToken.exp){
      localStorage.removeItem('userToken');
      this.route.navigate(['/','login']);
    }
    return true;
  };

  selectSliderHandler(event: any) {
    this.progress = event.target.value;
  }

  addLaporan() {
    this.tempNumberLaporan1++
    this.templaporan = { id: this.tempNumberLaporan1, taskName: '', taskDescription: '', progressTask: 0, sliderStatus: false}
    this.tempFinal.objectLaporan[this.loadedData].objectTasks.push(this.templaporan);
  }

  addProject() {
    this.laporan = [{
      id: 1,
      taskName: '',
      taskDescription: '',
      progressTask: 0,
      sliderStatus: false,
    }];
    this.projectName = '';
    this.roleName = '';
    this.loadedData = this.tempFinal.objectLaporan.length;
    this.tempProject = { id: this.loadedData, projectId: 0, roleId: 0, objectTasks: this.laporan }
    this.tempFinal.objectLaporan.push(this.tempProject);
    console.log(this.loadedData);
  }

  addNama(projectId: number, id: number, event: any){
    this.tempFinal.objectLaporan[projectId].objectTasks.find(laporan => {
      if (laporan.id == id) {
        laporan.taskName = event.target.value;
        return true;
      }
    
    });
  }

  addDescription(projectId: number, id: any, event: any){
    this.tempFinal.objectLaporan[projectId].objectTasks.find(laporan => {
      if (laporan.id == id) {
        laporan.taskDescription = event.target.value;
      }
    })
  }

  changeRoleName(event: any, id: number){
    this.roleName = event.target.value
    this.allRole.find(role => {
      if (role.rolename == event.target.value){
      this.formRoleId = role.roleid
      }
    })
    this.statusData = {
      userId : this.accessToken.id[0].user_id,
      projectId : this.formProjectId,
      roleId: this.formRoleId,
    }
    this.submitSvc.getUdahLakuin(this.statusData).subscribe( data => {
      this.statusCount = data;
    });
    this.tempFinal.objectLaporan[id].roleId = this.formRoleId;
  }

  changeProjectName(event: any, id: number){
    this.projectName = event.target.value
    this.allProject.find(project => {
      if (project.projectname == event.target.value){
      this.formProjectId = project.projectid
      }
    })
    this.tempFinal.objectLaporan[id].projectId = this.formProjectId;

  }

  addProgress( projectId: number, id: number, event: any) {
    this.tempFinal.objectLaporan[projectId].objectTasks.find(laporan => {
      if (laporan.id == id) {
        laporan.progressTask = event.target.value;
        return true;
      }
    });
  }

  submitLaporan(){
    this.finalLaporan.userId = this.accessToken.id[0].user_id;
    this.finalLaporan.created_date = this.currentDate;
    for (this.i = 0; this.i < this.tempFinal.objectLaporan.length ; this.i++){
      for (this.j = 0; this.j < this.tempFinal.objectLaporan[this.i].objectTasks.length ; this.j++){
        this.objectTask = {
          taskName: this.tempFinal.objectLaporan[this.i].objectTasks[this.j].taskName,
          taskDescription: this.tempFinal.objectLaporan[this.i].objectTasks[this.j].taskDescription,
          progressTask: this.tempFinal.objectLaporan[this.i].objectTasks[this.j].progressTask,
        }
        this.tempObjectTask.push(this.objectTask);
      }
      this.objectLaporan = {
        projectId: this.tempFinal.objectLaporan[this.i].projectId,
        roleId: this.tempFinal.objectLaporan[this.i].roleId,
        objectTask: this.tempObjectTask,
      }
      this.tempObjectTask = [];
      this.tempObjectLaporan.push(this.objectLaporan);
    }
    this.finalLaporan.objectLaporan = this.tempObjectLaporan;
    this.submitSvc.submitReport(this.finalLaporan).subscribe( (data: any) => {;
      if(data.code === 201){
        this.route.navigate(['/','user','dashboard']);
    }
    },
    (err: HttpErrorResponse) => {
    });
  }
}
