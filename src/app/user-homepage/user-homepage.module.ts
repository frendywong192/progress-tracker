import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserDetailpageComponent } from './user-detailpage/user-detailpage.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserReportpageComponent } from './user-reportpage/user-reportpage.component';
import { UserEditReportComponent } from './user-edit-report/user-edit-report.component';


@NgModule({
  declarations: [UserDetailpageComponent, UserDashboardComponent, UserReportpageComponent, SharedModule, UserEditReportComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,

  ],
  exports: [
    UserDetailpageComponent
  ]
})
export class UserHomepageModule { }
