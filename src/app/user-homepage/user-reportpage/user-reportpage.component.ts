import { UserService } from './../../service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenData, ReportDetailTitle, ReportDetailDataSlug, ReportDetailSlug, ReportDetailData, ActiveClass } from './../../model/model';
import { Component, OnInit } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-user-reportpage',
  templateUrl: './user-reportpage.component.html',
  styleUrls: ['./user-reportpage.component.scss']
})
export class UserReportpageComponent implements OnInit {
  currentDate: string;
  currentTime: number;
  decoded: TokenData;
  constructor(
    private route: Router,
    private GetData : UserService,
    private router: ActivatedRoute
  ) { }

  

  projectName: string;
  roleName: string;
  titleData : ReportDetailTitle[];
  data: ReportDetailData[];

  i: number;
  titleDataSlug: ReportDetailSlug;
  detailDataSlug: ReportDetailDataSlug;
  pageSlug: string;

  class : ActiveClass[] = [];
  tempClass : ActiveClass;

  ngOnInit() {
    if(localStorage.getItem('userToken')){
      this.initialize();
    } else{
      this.route.navigate(['/','login']);
    }
  }


  initialize(){
    this.currentTime = new Date().getTime() / 1000;
    this.decoded= jwt_decode(localStorage.getItem('userToken'));
    if( this.currentTime > this.decoded.exp){
      localStorage.removeItem('userToken');
      this.route.navigate(['/','login']);
    }
    this.router.paramMap.subscribe(param => {
      this.pageSlug = param.get('slug');
      this.currentDate = this.pageSlug ;
      this.getDetailTitle(this.pageSlug, 0);
    });
  }

  getDetailTitle(data: string, loadedData: number){
    this.titleDataSlug = {
      created_date: data,
      userId: this.decoded.id[0].user_id,
    }
    this.GetData.getReportDetailTitle(this.titleDataSlug).subscribe( data => {
      this.titleData = data;
      for (this.i = 0; this.i < this.titleData.length ; this.i++) {
        this.tempClass = {
          class: 'deactivate'
        }
        this.class.push(this.tempClass)
        this.class[0].class = 'activate'
      }
      this.detailDataSlug = {
        created_date: this.pageSlug,
        userId: this.decoded.id[0].user_id,
        roleId: this.titleData[loadedData].role_id,
        projectId: this.titleData[loadedData].project_id,
      }
      this.projectName = this.titleData[0].project_name,
      this.roleName = this.titleData[0].role_name,
      this.getDetailData(this.detailDataSlug);
    });
  }
  
  getDetailData(data : ReportDetailDataSlug){
    this.GetData.getReportDetailData(data).subscribe( report => {
      this.data = report;
      console.log(this.data)
    })
  }

  chooseData(data: ReportDetailTitle, i: number){
    this.projectName = this.titleData[i].project_name,
    this.roleName = this.titleData[i].role_name,
    this.detailDataSlug = {
      created_date: this.pageSlug,
      userId: this.decoded.id[0].user_id,
      roleId: data.role_id,
      projectId: data.project_id
    }
    this.getDetailData(this.detailDataSlug);
    for(this.i = 0 ; this.i < this.class.length ; this.i++){
      this.class[this.i].class = 'deactivate';
    }
    this.class[i].class = 'activate';
  }

  edit(){
    this.route.navigate(['/user/homepage/editreport',this.pageSlug])
  }
}
