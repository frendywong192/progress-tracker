import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReportpageComponent } from './user-reportpage.component';

describe('UserReportpageComponent', () => {
  let component: UserReportpageComponent;
  let fixture: ComponentFixture<UserReportpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReportpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReportpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
